pub(crate) mod forms {
    use rocket::form::{Error, Errors, FromForm};

    fn duration_helper<'a>(_errors: Errors<'_>) -> Errors<'a> {
        Error::validation("Please enter number bigger than 1").into()
    }

    #[derive(Debug, FromForm, Clone)]
    pub(crate) struct FormIngredient<'v> {
        #[field(validate = range(1..))]
        pub(crate) r#ingredient_amount: u32,

        #[field(validate = len(1..=10))]
        pub(crate) r#ingredient_unit: &'v str,

        #[field(validate = len(1..=25))]
        pub(crate) r#ingredient_kind: &'v str,
    }

    #[derive(Debug, FromForm, Clone)]
    pub(crate) struct FormStep<'v> {
        #[field(validate = len(1..=200))]
        pub(crate) r#step: &'v str,
    }

    #[derive(Debug, FromForm)]
    pub(crate) struct NewRecipeSubmit<'v> {
        #[field(validate = len(1..=50))]
        pub(crate) r#title: &'v str,

        #[field(validate = len(1..=500))]
        pub(crate) notes: &'v str,

        #[field(validate = len(1..))]
        pub(crate) r#form_ingredients: Vec<FormIngredient<'v>>,

        #[field(validate = len(1..))]
        pub(crate) r#form_steps: Vec<FormStep<'v>>,

        #[field(validate = range(1..).map_err(duration_helper))]
        pub(crate) duration_mins: u32,
    }

    #[derive(Debug, FromForm)]
    pub(crate) struct UpdateLnAddressSubmit<'v> {
        #[field(validate = len(1..=50))]
        pub(crate) r#new_ln_address: &'v str,
    }
}

pub(crate) mod nostr {
    use serde::{Deserialize, Serialize};

    #[derive(Debug, Serialize, Deserialize, Clone)]
    pub struct Recipe {
        pub title: String,
        pub notes: String,
        pub ingredients: Vec<Ingredient>,
        pub steps: Vec<String>,
        pub duration_mins: u32,
    }

    #[derive(Debug, Serialize, Deserialize, Clone)]
    pub struct Ingredient {
        pub amount: u32,
        pub unit: String,
        pub kind: String,
    }
}
