# Guide

## Configure Recipistr

In `Rocket.toml` file you can change
* the relay used -> other relays may have different recipes
* your nostr private key -> allows you to use your own keys. If you don't add it new random nostr account will be created for you.


## Add LN address to your account

If you want to receive Lightning tips, make sure to add Lightning Address to your nostr profile:

- Go to your profile page
- Add your Lightning Address

Now others can directly support you.

#### How to tip recipe author

- Go to the page of a recipe you like
- Click on the public key of the author, to open the author page
- Recipe author needs to have Lightning address configured (you will see a QR code if he has)
- Scan the QR code with a Lightning wallet and choose the tip amount


## Browse recipes

On the main page
- Click on ingredient on the left, to see recipes using that ingredient.
  - You can choose as many or as little ingredients as you want.

    
## Create recipe

- Click the 'New recipe' link on top
- Fill out the form
- Click submit

After you click submit your computer will have to solve a problem and then your recipe is posted.
This is done to prevent people from posting spam recipes.
You don't have to do anything, just wait for the recipe to appear(it should take around 15 seconds).


## Anti-spam measures

To avoid spam, Recipistr uses Proof-of-Work:

1. When creating the recipe
2. When user views existing recipes

Both have the default of 10 bits of PoW and can be configured in `Rocket.toml`
The bigger the bits amount the longer it more time it takes for the computer to solve the problem.
